#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include "constants.h"

/**
 * Estrutura de dados que guarda os tipos de Control Field
 */
typedef enum {
    DISC, UA, SET, I, RR, REJ
} ControlFieldType;

/**
 * Estrutura de informação que guarda os estados da leitura
 */
typedef enum {
    START, FLAG_RCV, A_RCV, C_RCV, BCC_OK, STOP
} State;

/**
 * Estrutura de dados que guarda o tipo de erros
 * 
 * ERROR - qualquer erro de escrita ou leitura ou que aborte o programa
 * DATA ERROR - erro na interpretacao dos dados da trama I
 * OK - sem erros
 * EMPTY - nao esta a receber nada (evita ciclos infinitos)
 */
typedef enum {
    ERROR, DATAERROR, OK, EMPTY
} ReturnType;

/**
 * Estrutura de dados que guarda as informacoes recebidas da trama.
 * Guarda o tipo (Control Field)
 * Guarda o conteudo dos dados de uma mensagem e o respetivo tamanho (caso seja do tipo I);
 * Guarda uma flag que indica se a mensagem enviada e ou nao uma retransmissao;
 * Guarda o endereco de controlo (1 ou 3)
 */
typedef struct
{
    ControlFieldType type;
    unsigned char message[BUF_SIZE];        
    unsigned int message_size;    
    unsigned int isRetransmission;  
    unsigned char controlAdress;
} Message;

/**
 * Estrutura de informação de uma trama. 
 * Guarda a porta: Dispositivo /dev/ttySx, x = 0, 1;
 * a velocidade de transmissao; o Número de sequência da trama: 0, 1;
 * o Valor do temporizador; Número de tentativas em caso de falha; qual o modo de DEBUG.
 */
typedef struct
{
    char port[20];        			
    int baudRate;       			
    unsigned int sequenceNumber;    
    unsigned int timeout;  			
    unsigned int numTransmissions;  
    unsigned int mode;			
}dataLink;

/**
 * Estrutura que guarda as estatisticas.
 */
typedef struct
{
    unsigned int tramasIenviadas;
    unsigned int tramasIretransmitidas;
    unsigned int tramasIrecebidas;
    unsigned int timeouts;
    unsigned int REJenviados;
    unsigned int REJrecebidos;
}estatisticas;

//inicializacao de structs

static dataLink data_link;

estatisticas statistics;

//metodos do linkLayer.c

/**
 * Handler para o alarme.
 */
void handler();
 
/**
 * Inicializacao da struct data_link
 */
void init_linkLayer(unsigned char *port, unsigned int mode);

/**
 * Inicializa a struct newtio, e abre o file descriptor correspondente a porta.
 * @param port
 * @param flag TRANSMITTER / RECEIVER
 * @return identificador da ligação de dados ; valor negativo em caso de erro
 */
int llopen(unsigned char *port, int isReceiver);

 /** 
 * Metodo invocado pelo metodo llopen pela maquina emissora.
 * Cria e envia tramas do tipo SET e recebe do tipo UA.
 * Se recebida com sucesso, a comunicacao e estabelecida.
 */
int llopen_sender(int fd);

/**
 * Metodo invocado pelo llopen para a maquina recetora.
 * Recebe tramas do tipo SET e envia do tipo UA.
 */
int llopen_receiver(int fd);

/**
 * Usado pela maquina emissora. Recebe um pacote e cria uma trama do tipo I. De seguida envia-a e espera pela resposta RR(sucesso) ou REJ(erro no bcc2).
 * Ciclo protegido por timeouts e numero maximo de retransmisssoes.
 * @param fd identificador da ligação de dados
 * @param buffer array de caracteres a transmitir 
 * @param length  comprimento do array de caracteres 
 * @return número de caracteres escritos ; valor negativo em caso de erro
 */
int llwrite(int fd, unsigned char * buffer, int length);

/**
 * Metodo invocado pela maquina recetora. Recebe tramas do tipo I e cria/envia tramas do tipo RR ou REJ.
 * Ciclo protegido por um temporizador de tempo maximo disponivel para realizar o metodo.
 * @param fd identificador da ligação de dados
 * @param buffer array de caracteres recebidos 
 * @return comprimento do array (número de caracteres lidos) ; valor negativo em caso de erro
 */
int llread(int fd, unsigned char * buffer);

/**
 * Metodo que repoe os valores da estrutura.
 * @param fd File descriptor
 * @param isReceiver Flag que indica qual e a maquina (emissora ou recetora)
 * @return -1 se erro ou 0 se sucesso.
 */
int llclose(int fd, int isReceiver);

/**
 * Metodo invocado pelo llclose para a maquina recetora. 
 * Recebe uma trama do tipo DISC e confirma com o envio de outra do tipo DISC. No fim recebe uma trama do tipo UA.
 * @param fd 
 * @return 0 se sucesso ou -1 se tem Serros.
 */
int llclose_receiver(int fd);

/**
 * Metodo invocado pelo llclose para a maquina emissora. 
 * Envia tramas do tipo DISC, recebe outra do tipo DISC e de seguida recebe do tipo UA.
 * @param fd 
 * @return 0 se sucesso ou -1 se temerros.
 */
int llclose_sender(int fd);

//metodos do linkLayerAux.c

/**
 * Cria uma trama do tipo S ou U de acordo com o endereco de controlo.
 * @param flag Tipo de trama
 * @param flag_A endereco de controlo
 * @return trama criada
 */
unsigned char* build_frame_SU(ControlFieldType flag, unsigned char flag_A);

/**
 * Metodo que cria uma trama do tipo I
 * @param campo de dados.
 * @param tamanho do campo de dados.
 * @return FrameI : |F|A|C|BCC1|DATA|BCC2|F|
 */
unsigned char* build_frame_I(unsigned char* data, unsigned int data_length);

/**
 * Recebe uma trama de qualquer tipo byte a byte interpretada por uma maquina de estados.
 * Tramas com cabecalho errado sao ignoradas.
 * Se nao estiver a ler nada retorna EMPTY
 * @param fd File descriptor para ler da porta
 * @param msg Struct que sera preenchida neste metodo com todas as informacoes necessarias pelos metodos que a invocam.
 * @return ReturnType
 */
ReturnType receive(int fd, Message *msg);

/**
 * Metodo que retorna a flag correspondente ao tipo pedido.
 * @flag tipo de trama
 * @return caracter correspondente ao tipo de controlo
 */
unsigned char getControlField(ControlFieldType flag);

/**
 * Metodo que retorna o tipo de acordo com a flag recebida.
 * @param c caracter correspondente ao tipo
 * @return ControlFieldType
 */
ControlFieldType setControlField(unsigned char c);

/**
 * Metodo que aplica a tecnica byte Stuffing as tramas recebidas como argumento.
 * @param frame Trama recebida que será "stuffed" neste método
 * @param frame_length tamanho da trama antes do stuffing.
 * @return novo size da trama
 */
int stuff(unsigned char *frame, int frame_length);

/**
 * Processo contrario ao stuff.
 * @param frame Trama recebida que será "destuffed" neste método
 * @param frame_length tamanho da trama antes do destuffing.
 * @return novo size da trama
 */
int destuff(unsigned char *frame, int frame_length);
 
/**
 * So para debugging.
 * Faz o display completo da trama.
 */
void display(unsigned char *frame, int n);